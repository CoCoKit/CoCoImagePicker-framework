//
//  CoCoImagePickerMacros.h
//  WeiboCoCo
//
//  Created by 陈明 on 2016/12/28.
//  Copyright © 2016年 com.weibococo. All rights reserved.
//

#ifndef CoCoImagePickerMacros_h
#define CoCoImagePickerMacros_h

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    ImagePickerSelectedTypeMulti,
    ImagePickerSelectedTypeRadio,
} ImagePickerSelectedType;

typedef enum : NSUInteger {
    ImagePickerOperationNone = 0,
    ImagePickerOperationGoToSystemSettingPageShouldNotifation = 1 << 0,// 去系统页面设置权限后，App会重启，用户数据会丢失，所以需要提示用户保存
} ImagePickerOperation;

#endif /* CoCoImagePickerMacros_h */
