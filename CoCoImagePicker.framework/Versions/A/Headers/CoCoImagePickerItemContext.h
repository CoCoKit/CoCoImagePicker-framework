//
//  CoCoImagePickerItemContext.h
//  CoCoImagePicker
//
//  Created by 陈明 on 2018/6/14.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoImagePickerItemContext : NSObject
@property (nonatomic, strong) UIImage *selected_icon;
@property (nonatomic, strong) UIImage *one_selected_icon;
@property (nonatomic, strong) UIImage *unSelected_icon;
@end
