//
//  CoCoImagePickerController.h
//  WeiboCoCo
//
//  Created by 陈明 on 2016/12/27.
//  Copyright © 2016年 com.weibococo. All rights reserved.
//


#import "CoCoAsset.h"
#import "CoCoImagePickerMacros.h"

typedef void (^imagePickerFinishedWithRadio)(CoCoAsset *asset);
typedef void (^imagePickerFinishedWithMulti)(NSArray<CoCoAsset *> *assets);

@class CoCoImagePickerController;

@protocol CoCoImagePickerControllerDelegate <NSObject>
@optional
- (void)imagePicker:(CoCoImagePickerController *)picker finishedWithRadio:(CoCoAsset *)asset;
@optional
- (void)imagePicker:(CoCoImagePickerController *)picker finishedWithMulti:(NSArray<CoCoAsset *> *)assets;
@optional
- (void)imagePickerDidCancel:(CoCoImagePickerController *)picker;
@end

@interface CoCoImagePickerController : UIViewController
@property (nonatomic, weak) id<CoCoImagePickerControllerDelegate> delegate;
@property (nonatomic, assign) ImagePickerOperation operation;
@property (nonatomic, copy) imagePickerFinishedWithRadio radioBlock;
@property (nonatomic, copy) imagePickerFinishedWithMulti multiBlock;
- (instancetype)initWithMultiCount:(NSInteger)count;
- (instancetype)initWithRadio;
@end
