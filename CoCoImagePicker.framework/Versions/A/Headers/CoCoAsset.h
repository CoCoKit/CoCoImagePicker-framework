//
//  CoCoAsset.h
//  WeiboCoCo
//
//  Created by 陈明 on 2016/12/28.
//  Copyright © 2016年 com.weibococo. All rights reserved.
//

#import <Photos/Photos.h>

@interface CoCoAsset : NSObject
- (instancetype)initWithPHAsset:(PHAsset *)asset;
@property (nonatomic, strong, readonly) PHAsset *asset;
@property (nonatomic, assign, readonly) BOOL isSelecetd;
@property (nonatomic, assign, readonly) NSInteger indexNum;
@property (nonatomic, assign, readonly) UInt64 recordTime;

- (void)setIndex:(NSInteger)index;
- (void)markIsSelected:(BOOL)isSelected;
- (void)touched;
@end
