//
//  CoCoImagePickerItemNode.h
//  WeiboCoCo
//
//  Created by 陈明 on 2016/12/27.
//  Copyright © 2016年 com.weibococo. All rights reserved.
//

#import "CoCoImagePickerMacros.h"
#import <UIKit/UIKit.h>
#import "CoCoAsset.h"

@interface CoCoImagePickerItemNode : UICollectionViewCell
- (void)setupWithCoCoAsset:(CoCoAsset *)asset andSelectType:(ImagePickerSelectedType)type context:(id)context;
@end
