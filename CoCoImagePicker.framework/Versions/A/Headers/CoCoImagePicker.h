//
//  CoCoImagePicker.h
//  CoCoImagePicker
//
//  Created by 陈明 on 2017/6/15.
//  Copyright © 2017年 CoCo. All rights reserved.
//


#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#endif

#import "CoCoImagePickerMacros.h"
#import "CoCoImagePickerController.h"
#import "CoCoPhotoLibraryManager.h"



// ! Project version number for CoCoImagePicker.
FOUNDATION_EXPORT double CoCoImagePickerVersionNumber;

// ! Project version string for CoCoImagePicker.
FOUNDATION_EXPORT const unsigned char CoCoImagePickerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoCoImagePicker/PublicHeader.h>
