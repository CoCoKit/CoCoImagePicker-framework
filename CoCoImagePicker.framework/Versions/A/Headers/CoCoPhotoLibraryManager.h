//
//  SFPhotoLibraryManager.h
//  SoulFerry
//
//  Created by 陈明 on 16/9/7.
//  Copyright © 2016年 com.chenming.app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PHAsset;
typedef void (^SFPhotoLibraryManagerGetPhotosBlock)(NSArray <PHAsset *> *assets);
typedef void (^SFPhotoLibraryManagerNoAuthorizationBlock)(void);
typedef void (^SFPhotoLibraryManagerFieldBlock)(NSError *error);

@interface CoCoPhotoLibraryManager : NSObject
@property (nonatomic, copy) SFPhotoLibraryManagerGetPhotosBlock getPhotosFinishBlock;
@property (nonatomic, copy) SFPhotoLibraryManagerNoAuthorizationBlock noAuthorizationForPhotoBlock;
@property (nonatomic, copy) SFPhotoLibraryManagerNoAuthorizationBlock noAuthorizationForCameraBlock;
@property (nonatomic, copy) SFPhotoLibraryManagerFieldBlock fieldBlock;


- (void)getAllAssets; // 异步获取成功后，会调用getPhotosFinishBlock


- (void)savePhotoWithImage:(UIImage *)image completion:(void (^)(NSError *error))completion;

- (void)fetchImageInAsset:(PHAsset *)asset size:(CGSize)size isResize:(BOOL)isResize completeBlock:(void (^)(UIImage *image, NSDictionary *info))completeBlock;
@end
